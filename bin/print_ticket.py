import json


class Transport(object):
    def __init__(self):
        import requests
        self.sess = requests.Session()

    def request(self, method, uri, headers, **kwargs):
        response = self.sess.request(method, uri, headers=headers, **kwargs)
        if response.status_code == 204:
            return True
        if not response.ok:
            raise Exception(response)
        if 'results' in response.json():
            return response.json()['results']
        return response.json()


if __name__ == '__main__':
    transport = Transport()
    response = transport.request(
        method='POST',
        uri='https://checkin.congressy.com/queues/items',
        headers={
            'PIN': '123123',
            'Content-Type': 'application/json',
        },
        data=json.dumps({
            "event_id": 8,
            "service_id": 366,
            "subscription_id": "6f11ebbd-2b74-4591-bf7d-8b59257dada0",
            "printer_number": 1
        })
    )
    print(response)
