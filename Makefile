.PHONY: reset
reset:
	rm -rf congressy_sdk.egg-info build .setup.py.swp setuptools

clean: reset
	rm -rf dist

.PHONY: setup
setup:
	python3 setup.py sdist bdist_wheel
	@make reset

.PHONY: publish
publish: setup
	twine upload dist/*
	@make clean
